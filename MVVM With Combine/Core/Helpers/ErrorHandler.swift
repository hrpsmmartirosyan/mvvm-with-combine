//
//  ErrorHandler.swift
//  MVVM with Combine
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import UIKit

public class ErrorHandler {
    public func handleError(_ error: Error) -> UIAlertController? {
        //Here we can handle different type of errors, for this case i'm showing just network error messgae
        let alert = UIAlertController(title: "network.error.title".localized, message: "network.error.message".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "error.message.ok.title".localized, style: .default, handler: nil))

        return alert
    }
}


