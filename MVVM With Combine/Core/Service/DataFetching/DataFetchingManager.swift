//
//  DataFetchingManager.swift
//  MVVM with Combine
//
//  Created by hripsimem on 11/30/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import Combine

class DataFetchingManager {
    //Network manager to fetch data from data source
    //Will be injected with constructor injection
    let networkManager: NetworkManager

    //Used to add all disposables to dispose them after object dealocation
    init(with networkManager: NetworkManager) {
        self.networkManager = networkManager
    }

    func execute<T: Decodable, R: Routing, E: Error>(_ route: R, errorType: E.Type) -> AnyPublisher<T, Error> {

        return self.networkManager.fetch(route)
    }
}

