//
//  BaseViewController.swift
//  MVVM with Combine
//
//  Created by hripsimem on 12/3/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//
import UIKit
import Combine

open class BaseViewController<T: ViewModel>: UIViewController, ViewModelled {
    open var viewModel: T?
    lazy open var errorHandler = ErrorHandler()
    var cancellable = Set<AnyCancellable>()

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    override open func viewDidLoad() {
        super.viewDidLoad()

        if let vm = self.viewModel {
            self.bindViewModel(vm)
            self.bindError(vm)
        }
    }

    // VM Binding
    open func bindViewModel(_ viewModel: T) {}

    open func bindError(_ viewModel: T) {
        self.viewModel?.$errorPublisher.sink { [weak self] error  in
            guard let error = error else { return }

            self?.handleError(error)
        }.store(in: &cancellable)
    }

    open func handleError(_ error: Error) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            guard let errorMessage = self.errorHandler.handleError(error) else { return }

            self.present(errorMessage, animated: true, completion: nil)
        }
    }
}
