//
//  ViewModel.swift
//  MVVM with Combine
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import Combine

open class ViewModel: NSObject {
    @Published public var errorPublisher: Error?

    var cancellable = Set<AnyCancellable>()

    open func handleSuccess(_ data: Any) -> Error? {
        return nil
    }

    open func handleError(_ error: Error) {
        errorPublisher = error
    }
}

