//
//  CountryListViewController.swift
//  MVVM with Combine
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import UIKit
import Combine

final class CountryListViewController: BaseViewController<CountryListViewModel>, UITableViewDelegate, UITableViewDataSource {

    // MARK: Private members
    private var countryLisTableView = UITableView()
    private var activityIndicator: UIActivityIndicatorView!

    // MARK: - View life cycle
    override public func viewDidLoad() {
        super.viewDidLoad()

        self.title = "country.list.title".localized
        self.setupTableView()
        self.viewModel?.fetchCountryList()

        self.setupLoader()
    }

    // MARK: - ViewModel life cycle
    public override func bindViewModel(_ viewModel: CountryListViewModel) {
        viewModel.reloadData.sink(receiveValue: { [weak self] reloadData in
            if reloadData {
                self?.countryLisTableView.reloadData()
            }
            self?.showLoading(!reloadData)
        }).store(in: &cancellable)
    }

    // MARK: - TableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let dataSource = self.viewModel?.dataSource else { return 0 }

        return dataSource.count
    }

    // MARK: - TableView delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CountryCell = tableView.dequeueReusableCell(for: indexPath)

        guard let cellModel = viewModel?.dataSource[indexPath.row] else { return UITableViewCell() }

        cell.setup(with: cellModel)

        return cell
    }

    // MARK: Private helpers
    private func setupTableView() {
        countryLisTableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        countryLisTableView.delegate = self
        countryLisTableView.dataSource = self
        countryLisTableView.separatorStyle = .none
        countryLisTableView.registerCell(with: CountryCell.self)

        self.view.addSubview(countryLisTableView)
    }

    private func setupLoader() {
        activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.startAnimating()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)

        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }

    private func showLoading(_ show: Bool) {
        show ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
    }
}

