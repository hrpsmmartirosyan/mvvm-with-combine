//
//  CountryListViewModel.swift
//  MVVM with Combine
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import Combine

final class CountryListViewModel: ViewModel {
    // MARK: Public members
    var reloadData = PassthroughSubject<Bool, Never>()
    var dataSource = [CountryCellPresentationModel]()

    // MARK: Private members
    private let webService = CountryListWebService(with: NetworkManager())

    //MARK: Public API
    func fetchCountryList() {
        let parameters = CountryListParameters()

        webService
            .call(with: parameters)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self.handleError(error)
                }
            }, receiveValue: { [weak self] (list) in
                let countries = list.countries.map { CountryCellPresentationModel(name: $0.name, description: $0.description) }
                self?.dataSource = countries
                self?.reloadData.send(true)
            }).store(in: &cancellable)
    }
}



