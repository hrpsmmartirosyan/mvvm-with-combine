//
//  CountryListParameters.swift
//  MVVM with Combine
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation

public struct CountryListParameters {
    public init() {}
}

extension CountryListParameters: Routing {
    var method: RequestType {
        return .GET
    }
    
    var encoding: ParameterEncoding {
        return .json
    }
    
    var routPath: String {
        return "get/NkVPk2eoF"
    }
}


